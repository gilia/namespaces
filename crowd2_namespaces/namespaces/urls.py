from django.urls import include, path
from rest_framework import routers
from .views import PrefixViewSet, UserViewSet


by_user = PrefixViewSet.as_view({
    'get': 'by_user'
})


router = routers.DefaultRouter()
router.register(r'prefixs', PrefixViewSet)
router.register(r'users', UserViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('prefixs/by_user/<int:userid>', by_user, name="prefixs-by_user")
]
