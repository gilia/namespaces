# from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from django.contrib.auth.models import User
from .models import Prefix
from .serializers import PrefixSerializer, UserSerializer


# Create your views here.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class PrefixViewSet(viewsets.ModelViewSet):
    queryset = Prefix.objects.all()
    serializer_class = PrefixSerializer

    @action(detail=True)
    def by_user(self, request, userid):
        prefixes = Prefix.objects.filter(user=userid)
        serializer = PrefixSerializer(prefixes, many=True,
                                      context={'request': request})
        return Response(serializer.data)
