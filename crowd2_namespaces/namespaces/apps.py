from django.apps import AppConfig


class NamespacesConfig(AppConfig):
    name = 'namespaces'
