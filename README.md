
# Installation
- Create a Virtualenv: `virtualenv .virtualenv`
- Activate environment: `source .virtualenv/bin/activate.fish`
- Install python requirements: `pip install -r requirements.txt`
